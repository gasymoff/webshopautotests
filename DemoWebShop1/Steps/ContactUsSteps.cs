﻿using DemoWebShop.Implements.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class ContactUsSteps
    {
        [Then(@"Successful message with text '(.*)' is  displayed")]
        public void ThenSuccessfulMessageWithTextIsDisplayed(string message)
        {
            Assert.AreEqual(message.ToLower(), GeneralInterfaces.ContactUsGui.MessageisSended());
        }

        [When(@"User sent '(.*)' generate message written on ContactUs Module")]
        public void WhenUserSentGenerateMessageWrittenOnContactUsModule(string messageText)
        {
            switch (messageText)
            {
                case "random":
                    GeneralInterfaces.ContactUsGui.SendRandomGenerateMessage();
                    break;
                default:
                    GeneralInterfaces.ContactUsGui.SendMessageText(messageText);
                    break;
            }
        }
    }
}
