﻿using DemoWebShop.Implements.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class RegistrationSteps
    {
        [When(@"I fill the form with data")]
        public void WhenIFillTheFormWithData(Table table)
        {
            GeneralInterfaces.RegistrationGui.RegistrationUserWithData(
                table.Rows[0]["gender"],
                table.Rows[0]["fname"],
                table.Rows[0]["lname"],
                table.Rows[0]["email"],
                table.Rows[0]["password"],
                table.Rows[0]["confirm_password"]);
        }

        [Then(@"I registered on the web page")]
        public void ThenIRegisteredOnTheWebPage() => Assert.AreEqual(true, GeneralInterfaces.RegistrationGui.RegistrationResultPageIsOpened());

        [When(@"I fill the form with invalid data")]
        public void WhenIFillTheFormWithInvalidData(Table table)
        {
            GeneralInterfaces.RegistrationGui.RegistrationUserWithData(
               table.Rows[0]["gender"],
               table.Rows[0]["fname"],
               table.Rows[0]["lname"],
               table.Rows[0]["email"],
               table.Rows[0]["password"],
               table.Rows[0]["confirm_password"]);
        }

        [Then(@"Error message with text (.*) is displayed")]
        public void ThenErrorMessageWithTextIsDisplayed(string errorMessage)
        {
            switch (errorMessage)
            {
                case "First name is required.":
                    Assert.AreEqual(errorMessage, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessage));
                    break;
                case "Last name is required.":
                    Assert.AreEqual(errorMessage, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessage));
                    break;
                case "Wrong email":
                    Assert.AreEqual(errorMessage, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessage));
                    break;
                case "The password should have at least 6 characters.":
                    Assert.AreEqual(errorMessage, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessage));
                    break;
                case "The password and confirmation password do not match.":
                    Assert.AreEqual(errorMessage, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessage));
                    break;
                case "Password is required.":
                    Assert.AreEqual(errorMessage, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessage));
                    break;
                default:
                    break;
            }
        }
    }
}
