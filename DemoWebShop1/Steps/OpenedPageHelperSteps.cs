﻿using DemoWebShop.Implements.General;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class HelperSteps
    {
        [Given(@"The '(.*)' page is opened")]
        public void GivenThePageIsOpened(string webPage)
        {
            if (webPage == "Authorization")
            {

            }
            else if (webPage == "Registration")
            {
                GeneralInterfaces.RegistrationGui.OpenRegistrationPage();
            }
            else if (webPage == "Home")
            {
                GeneralInterfaces.ContactUsGui.UserIsAuthorized();
            }
        }
    }
}
