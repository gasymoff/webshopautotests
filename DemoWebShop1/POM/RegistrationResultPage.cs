﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class RegistrationResultPage : WebDriver
    {
        private static readonly By _registrationResultText = By.XPath("//div[@class= 'result']");

        public string RegistrationResultText() => FindElement(_registrationResultText).Text;
    }
}
