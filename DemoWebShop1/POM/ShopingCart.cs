﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class ShopingCart : WebDriver
    {
        private static readonly By _shopingCartItemName = By.XPath("//tr[@class = 'cart-item-row']/td/a");
        private static readonly By _shopingCartItemCheckBox = By.XPath("//input[@name='removefromcart']");
        private static readonly By _updateShoppingCartBtn = By.XPath("//div[@class= 'common-buttons']/input[@name = 'updatecart']");

        public string GetShopingCartItemName() => FindElement(_shopingCartItemName).Text.ToLower();

        public void ClickOnItemCheckBox() => FindElement(_shopingCartItemCheckBox).Click();

        public void ClickOnUpdateShoppingCart() => FindElement(_updateShoppingCartBtn).Click();
    }
}
