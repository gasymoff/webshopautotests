﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class BooksPage : WebDriver
    {
        private static readonly By _computingAndInternetBookBtn = By.XPath("//div[@data-productid= '13']//input[@type = 'button']");
        private static readonly By _fictionBookBtn = By.XPath("//div[@data-productid = '45']//div[@class= 'add-info']/div[@class = 'buttons']/input");
        private static readonly By _healthBookBtn = By.XPath("//div[@data-productid = '22']//div[@class= 'add-info']/div[@class = 'buttons']/input");

        private static readonly By _shopingCartNumber = By.XPath("//span[@class='cart-qty']");
        private static readonly By _shopingCartBtn = By.XPath("//a[@href  = '/cart']/span[@class = 'cart-label']");

        public void ClickOnComputingAndInternetBookBtn() => FindElement(_computingAndInternetBookBtn).Click();
        
        public void ClickOnFictionBookBtn() => FindElement(_fictionBookBtn).Click();
        
        public void ClickOnHealtBookBtn() => FindElement(_healthBookBtn).Click();

        public void ClickOnShopingCartBtn() => FindElement(_shopingCartBtn).Click();

        public string CheckShopingCartNumber() => FindElement(_shopingCartNumber).Text.Substring(1,1);    
    }
}
