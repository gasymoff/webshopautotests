﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{

    public class ContactUsPage : WebDriver
    {

        private static readonly By _enteredEnquiry = By.Id("Enquiry");
        private static readonly By _submitBtn = By.XPath("//input[@class='button-1 contact-us-button']");
        private static readonly By _sentMessageText = By.XPath("//div[@class='result']");
        private static readonly By _fullNameField = By.Id("FullName");
        private static readonly By _emailField = By.Id("Email");

        public void EnterGenerateMessage() => FindElement(_enteredEnquiry).SendKeys(HelperMethods.GetMessage(10));

        public void EnterUserMessage(string message) => FindElement(_enteredEnquiry).SendKeys(message);
     
        public void ClickOnSubmitBtn() => FindElement(_submitBtn).Click();

        public string SentMessageText() => FindElement(_sentMessageText).Text.ToLower();

        public void EnterFullName() 
        {
            FindElement(_fullNameField).SendKeys("test test");
        }
        public void EnterEmail()
        {
            FindElement(_emailField).SendKeys("temp01@gmail.com");
        }


    }
}
