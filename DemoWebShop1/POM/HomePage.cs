﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class HomePage : WebDriver
    {
        private static readonly By _registrationBtn = By.XPath("//a[@class = 'ico-register']");
        private static readonly By _contactUsBtn = By.XPath("//a[@href='/contactus']");
        private static readonly By _bookCategoryBtn = By.XPath("//a[@href='/books']");
        

        public void ClickOnRegistrationBtn() => FindElement(_registrationBtn).Click();

        public void OpenHomePage() => Navigate().GoToUrl("http://demowebshop.tricentis.com/");

        public void ClickOnContactUsBtn() => FindElement(_contactUsBtn).Click();

        public void ClickOnBookCategory() => FindElement(_bookCategoryBtn).Click();
        
    }
}
