﻿Feature: RegistrationFeature
	In order to buy computer items
	As a user 
	I want to registered 

@mytag
Scenario: It is possible to registered with valid data
	Given The 'Registration' page is opened
	When I fill the form with data
		| gender | fname | lname | email              | password  | confirm_password |
		| male   | test  | test  | temp9933@gmail.com | Testtest1 | Testtest1        |
	Then I registered on the web page

@mytag
Scenario Outline: It is impossible to registered with invalid data
	Given The 'Registration' page is opened
	When I fill the form with invalid data
		| gender   | fname   | lname   | email   | password   | confirm_password   |
		| <gender> | <fname> | <lname> | <email> | <password> | <confirm_password> |
	Then Error message with text '<messageText>' is displayed

	Examples:
		| gender | fname       | lname | email      | password | confirm_password | messageText                                          |
		| male   |             | test  | randomMail | testtest | testtest         | First name is required.                              |
		| male   | test        |       | randomMail | testtest | testtest         | Last name is required.                               |
		|        |             |       |            |          |                  | First name is required.                              |
		| female | unique name | test  | testt      | testtest | testtest         | Wrong email                                          |
		| male   | test        | test  | @gmail.com | testtest | testtest         | Wrong email                                          |
		| female | test        | test  | randomMail | test     | test             | The password should have at least 6 characters.      |
		| female | test        | test  | randomMail | testtest | test             | The password and confirmation password do not match. |
		| female | test        | test  | randomMail | testtest |                  | Password is required.                                |
		| female | test        | test  | randomMail |          | testtest         | Password is required.                                |
#kak zamenit email