﻿Feature: AddToCartFeature
	In order to buy some items on web application
	As a user
	I want to add to cart an item

@mytag
Scenario Outline: It is possible to add to cart an item
	Given The 'Home' page is opened
	When User add to cart a '<book>'  book 
	Then '<book>' has been added to the cart
	Examples: 
	| book                   | 
	| Computing and Internet | 
	| Fiction                | 
	| Health Book            | 