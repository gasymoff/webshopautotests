﻿using DemoWebShop.Interfaces;
using DemoWebShop.POM;

namespace DemoWebShop.Implements
{
    public class PurchaseGui : IPurchase
    {
        GiftCardPage giftcardPage;
        AddGiftCardPage addgiftcardPage;
        BookPage bookPage;
        LaptopPage laptopPage;
        DenimPage denimPage;
        DiamondPage diamondPage;
        ShoppingCartPage shoppingcartPage;
        CheckoutPage checkoutPage;
        ThankYouPage thankYouPage;

        public PurchaseGui()
        {
            giftcardPage = new GiftCardPage();
            addgiftcardPage = new AddGiftCardPage();
            bookPage = new BookPage();
            laptopPage = new LaptopPage();
            denimPage = new DenimPage();
            diamondPage = new DiamondPage();
            checkoutPage = new CheckoutPage();
            shoppingcartPage = new ShoppingCartPage();
            thankYouPage = new ThankYouPage();
        }

        public void AddingItemsToTheShopCart()
        {
            giftcardPage.OpenCardsPage();
            giftcardPage.ClickOnCard100();
            addgiftcardPage.SendDataToRecipientsNameField();
            addgiftcardPage.ClickOnAddToCartBtn();
            bookPage.OpenBookPage();
            bookPage.AddBookToCart();
            laptopPage.OpenNotebooksPage();
            laptopPage.AddToCartClick();
            denimPage.OpenShoesPage();
            denimPage.ClickOnAddtoCard();
            diamondPage.OpenDiaomondPage();
            diamondPage.ClickOnAddtoCard();
            shoppingcartPage.OpenShoppingCartPage();
            shoppingcartPage.ClickOnAgreeTermsBtn();
            shoppingcartPage.ClickOnCheckoutBtn();
        }

        public void FillBillingData(string firstName, string lastName, string companyName,
            string cityName, string firstAdress, string secondAddress,
            string postalCode, string phoneNumber, string faxNumber)
        {
                checkoutPage.EnterFirstName(firstName);
                checkoutPage.EnterLastName(lastName);
                checkoutPage.EnterCompanyName(companyName);
                checkoutPage.SelectCountryName();
                checkoutPage.EnterCityName(cityName);
                checkoutPage.EnterFirstAddress(firstAdress);
                checkoutPage.EnterSecondAddress(secondAddress);
                checkoutPage.EnterPostalCode(postalCode);
                checkoutPage.EnterPhoneNumber(phoneNumber);
                checkoutPage.EnterFaxNumber(faxNumber);
                checkoutPage.ClickOnFirstContinueBtn();
                checkoutPage.ClickOnSecondContinueBtn();
                checkoutPage.ClickOnGroundBtn();
                checkoutPage.ClickOnThirdContinueBtn();
                checkoutPage.ClickOnSelectCreditCardBtn();
                checkoutPage.ClickOnFourthContinueBtn();
        }
        
        public void FillCardData(string cardHolderName, string cardNumber, string cardCode)
        {
            checkoutPage.EnterCardHolderName(cardHolderName);
            checkoutPage.EnterCardNumber(cardNumber);
            checkoutPage.EnterCardCode(cardCode);
            checkoutPage.ClickOnFifthContinueBtn();
            checkoutPage.ClickOnConfirmBtn();
        }
        
        public string MessageInfo(string messageText)
        {
            return thankYouPage.ThankYouMessageText(messageText);
        }
    }
}
