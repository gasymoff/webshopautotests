﻿using DemoWebShop.Interfaces;

namespace DemoWebShop.Implements.General
{
    public static class GeneralInterfaces
    {
        public static IRegistration RegistrationGui { get; } = new RegistrationGui();

        public static IAddToCart AddToCartGui { get; } = new AddToCartGui();

        public static IContactUs ContactUsGui { get; } = new ContactUsGui();

        public static ILinks LinksGUI { get; } = new LinksGUI();

        public static IPriceInCompareListSteps PriceInCompareListGUI { get; } = new PriceInCompareListGUI();

        public static IAuthorization AuthorizationGui { get; } = new AuthorizationGui();

        public static IPurchase PurchaseGui { get; } = new PurchaseGui();
    }
}
