﻿using DemoWebShop.Interfaces;
using DemoWebShop.POM;

namespace DemoWebShop.Implements
{
    public class AuthorizationGui : IAuthorization
    {
        HomePage homepage;
        AuthorizationPage authorizationPage;
        AuthorizationResultPage authorizationResultPage;

        public AuthorizationGui()
        {
            homepage = new HomePage();
            authorizationPage = new AuthorizationPage();
            authorizationResultPage = new AuthorizationResultPage();
        }

        public void AuthorizationPageIsOpened()
        {
            homepage.OpenHomePage();
            homepage.ClickOnAuthorizationBtn();
        }

        public string AuthorizationResultPageIsOpened()
        {
            return authorizationResultPage.AuthorizationResultText();
        }

        public void AuthorizationUserWithData(string email, string password)
        {
            authorizationPage.InputEmail(email);
            authorizationPage.InputPassword(password);
            authorizationPage.ClickOnLogInBtn();
        }

        public string ErrorMessageIsDisplayed(string messageText)
        {
            switch (messageText.ToLower())
            {
                case "the credentials provided are incorrect.":
                case "no customer account found.":
                    return authorizationPage.InvalidLogIn();
                case "please enter a valid email address.":
                    return authorizationPage.InvalidEmail();
            }
            return null;
        }
    }
}