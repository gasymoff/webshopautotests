﻿using DemoWebShop.Helpers;
using DemoWebShop.Interfaces;
using DemoWebShop.POM;

namespace DemoWebShop.Implements
{
    public class ContactUsGui : IContactUs
    {
        AuthorizationHelper authorizationHelper;
        HomePage homePage;
        ContactUsPage contactUsPage;

        public ContactUsGui()
        {
            authorizationHelper = new AuthorizationHelper();
            homePage = new HomePage();
            contactUsPage = new ContactUsPage();
        }

        public string MessageisSended() => contactUsPage.SentMessageText();

        public void SendMessageText(string messageText)
        {
            contactUsPage.EnterUserMessage(messageText);
            contactUsPage.ClickOnSubmitBtn();
        }

        public void SendRandomGenerateMessage()
        {
            contactUsPage.EnterGenerateMessage();
            contactUsPage.ClickOnSubmitBtn();
        }

        public void UserIsAuthorized()
        {
            authorizationHelper.OpenHomePage();
            authorizationHelper.ClickOnAuthorizationBtn().InputEmail().InputPassword().ClickOnLogInBtn();
            homePage.ClickOnContactUsBtn();
        }
    }
}
