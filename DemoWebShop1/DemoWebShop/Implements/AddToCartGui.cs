﻿using DemoWebShop.Interfaces;
using DemoWebShop.POM;

namespace DemoWebShop.Implements
{
    public class AddToCartGui : IAddToCart
    {
        HomePage homePage;
        BooksPage booksPage;
        ShopingCart shopingCart;

        public AddToCartGui()
        {
            homePage = new HomePage();
            booksPage = new BooksPage();
            shopingCart = new ShopingCart();
        }

        public void AddToCartItem(string book)
        {
            homePage.ClickOnBookCategory();
            switch (book.ToLower())
            {
                case "computing and internet":
                    booksPage.ClickOnComputingAndInternetBookBtn();
                    break;
                case "fiction":
                    booksPage.ClickOnFictionBookBtn();
                    break;
                case "health book":
                    booksPage.ClickOnHealtBookBtn();
                    break;
            }
            booksPage.ClickOnShopingCartBtn();
        }

        public string CheckTheNameOfBook()
        {
            return shopingCart.GetShopingCartItemName();
        }

        public void ClearShopingCart()
        {
            shopingCart.ClickOnItemCheckBox();
            shopingCart.ClickOnUpdateShoppingCart();
        }
    }
}
