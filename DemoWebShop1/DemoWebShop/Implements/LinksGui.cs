﻿using DemoWebShop.Interfaces;
using DemoWebShop.POM;

namespace DemoWebShop.Implements
{
    public class LinksGUI : ILinks
    {
        HomePage homePage;
        FacebookPage facebookPage;
        TwitterPage twitterPage;
        YoutubePage youtubePage;

        public LinksGUI()
        {
            homePage = new HomePage();
            facebookPage = new FacebookPage();
            twitterPage = new TwitterPage();
            youtubePage = new YoutubePage();
        }
        
        public void OpenWebShopHomePage() => homePage.OpenHomePage();

        public bool CheckClickedLinksIsOpened(string link)
        {
            if (link.ToLower() == "facebook")
            {
                return facebookPage.CheckFacebookLink();
            }
            else if (link.ToLower() == "twitter")
            {
                return twitterPage.CheckTwitterLink();
            }
            else if (link.ToLower() == "youtube")
            {
                return youtubePage.CheckYoutubeLink();
            }
            return false;
        }

        public void FollowToSocialLink(string link)
        {
            if (link.ToLower() == "facebook")
            {
                homePage.ClickFacebookLink();
            }
            else if (link.ToLower() == "twitter")
            {
                homePage.ClickOnTwitterLink();
            }
            else if (link.ToLower() == "youtube")
            {
                homePage.ClickOnYoutubeLink();
            }
        }
    }
}