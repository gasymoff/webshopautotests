﻿using DemoWebShop.Interfaces;
using DemoWebShop.POM;

namespace DemoWebShop.Implements
{
    public class PriceInCompareListGUI : IPriceInCompareListSteps
    {
        HomePage homePage;
        CompareListPage compareListPage;
        SimpleComputerPage simpleComputerPage;
        DesktopsPage desktopsPage;
        ComputersPage computersPage;

        public PriceInCompareListGUI()
        {
            homePage = new HomePage();
            compareListPage = new CompareListPage();
            simpleComputerPage = new SimpleComputerPage();
            desktopsPage = new DesktopsPage();
            computersPage = new ComputersPage();
        }

        public void OpenWebShopHomePage() => homePage.OpenHomePage();

        double a = 0;
        double b = 0;

        public void AddDesktopToCompareListAfterSortingZA()
        {
            homePage.ClickOnComputersBtn();
            computersPage.ClickOnDesktopsBtn();
            desktopsPage.SelectSortingList();
            desktopsPage.SelectFirstComputerAfterSortingZtoA();
            a = simpleComputerPage.GetPrice();
            simpleComputerPage.SelectAddToCompareList();
            b = compareListPage.GetPriceInCompareList();
        }

        public bool CheckPriceInCompareListIsCorrect()
        {
            if (a == b)
            {
                return true;
            }
            else
                return false;
        }
    }
}