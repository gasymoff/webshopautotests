﻿using System;

namespace DemoWebShop.Helpers
{
    public static class HelperMethods
    {
        public static string GetUniqueMessage(int wordNumber)
        {
            string word = "qwertyuiop[]';lkjhgfdsazxcvbnm,./";
            string message = "";
            Random random = new Random();
            for (int i = 0; i < wordNumber; i++)
            {
                message += word[random.Next(31)];
            }
            return message;
        }

        public static string GetUniqueEmail(int charNumber) 
        {
            string chars = "qwertyuioplkjhgfdsazxcvbnm";
            string email = "";
            Random random = new Random();
            for (int i = 0; i < charNumber; i++)
            {
                email += chars[random.Next(0, 24)];
            }
            return email + "@gmail.com";
        }
    }
}