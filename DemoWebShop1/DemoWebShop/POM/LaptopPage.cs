﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class LaptopPage : WebDriver
    {
        private static readonly By _addToCartBtn = By.XPath("//input[@class='button-2 product-box-add-to-cart-button']");
        private string _laptopPageUrl = "http://demowebshop.tricentis.com/notebooks";

        public void OpenNotebooksPage() => Navigate().GoToUrl(_laptopPageUrl);
        
        public void AddToCartClick() => FindElement(_addToCartBtn).Click();
    }
}
