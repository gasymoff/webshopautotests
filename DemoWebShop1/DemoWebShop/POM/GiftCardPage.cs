﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class GiftCardPage : WebDriver
    {
        private string _giftCardsPageUrl = "http://demowebshop.tricentis.com/gift-cards";
        private static readonly By _giftCard = By.XPath("//img[@alt='Picture of $100 Physical Gift Card']");       
        
        public void ClickOnCard100() => FindElement(_giftCard).Click();

        public void OpenCardsPage() => Navigate().GoToUrl(_giftCardsPageUrl);
    }
}
