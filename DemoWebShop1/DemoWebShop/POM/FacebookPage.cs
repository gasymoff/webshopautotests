﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;

namespace DemoWebShop.POM
{
    class FacebookPage : WebDriver
    {
        private static readonly By _facebookLinkCheck = By.XPath("//*[@class='menu_login_container rfloat _ohf']/form[@id='login_form']");

        public bool CheckFacebookLink()
        {
            ReadOnlyCollection<String> tabs2 = new ReadOnlyCollection<String>(WindowHandles);
            SwitchTo().Window(tabs2[1]);
            return FindElement(_facebookLinkCheck).Displayed;
        }
    }
}