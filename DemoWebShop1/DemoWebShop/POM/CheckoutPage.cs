﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class CheckoutPage : WebDriver
    {
        private static readonly By _firstNameInputField = By.XPath("//input[@id='BillingNewAddress_FirstName']");
        private static readonly By _lastNameInputField = By.XPath("//input[@id='BillingNewAddress_LastName']");
        private static readonly By _emailInputField = By.XPath("//input[@id='BillingNewAddress_Email']");
        private static readonly By _companyInputField = By.XPath("//input[@id='BillingNewAddress_Company']");
        private static readonly By _selectCountry = By.XPath("//select[@id='BillingNewAddress_CountryId']");        
        private static readonly By _cityInputField = By.XPath("//input[@id='BillingNewAddress_City']");
        private static readonly By _newAdressOne = By.XPath("//input[@id='BillingNewAddress_Address1']");
        private static readonly By _newAdressTwo = By.XPath("//input[@id='BillingNewAddress_Address2']");
        private static readonly By _zipCode = By.XPath("//input[@id='BillingNewAddress_ZipPostalCode']");
        private static readonly By _phoneNumberInputField = By.XPath("//input[@id='BillingNewAddress_PhoneNumber']");
        private static readonly By _faxNumberInputField = By.XPath("//input[@id='BillingNewAddress_FaxNumber']");
        private static readonly By _billingContinueBtn = By.XPath("//div[@id='billing-buttons-container']//input[@class='button-1 new-address-next-step-button']");
        private static readonly By _shippingContinueBtn = By.XPath("//div[@id='shipping-buttons-container']/input[@class ='button-1 new-address-next-step-button']");
        private static readonly By _groundBtn = By.XPath("//div[@class='method-name']//input[@id='shippingoption_0']");
        private static readonly By _shippingMethodContinueBtn = By.XPath("//input[@class ='button-1 shipping-method-next-step-button']");
        private static readonly By _creditcardBtn = By.XPath("//input[@id ='paymentmethod_2']");
        private static readonly By _paymentMethodContinueBtn = By.XPath("//input[@class ='button-1 payment-method-next-step-button']");
        private static readonly By _selectCreditCardBtn = By.XPath("//input[@id ='paymentmethod_2']");
        private static readonly By _cardHolderNameInputField = By.XPath("//input[@id ='CardholderName']");
        private static readonly By _cardNumberInputField = By.XPath("//input[@id ='CardNumber']");        
        private static readonly By _cardCodeInputField = By.XPath("//input[@id ='CardCode']");
        private static readonly By _paymentInfoContinueBtn = By.XPath("//input[@class ='button-1 payment-info-next-step-button']");
        private static readonly By _confirmBtn = By.XPath("//input[@class ='button-1 confirm-order-next-step-button']");
        private static readonly By _billingAddressChecking = By.XPath("//select[@id='billing-address-select']/option[@value = '2193905']");

        public bool CheckingOfBillingAddress() => FindElement(_billingAddressChecking).Displayed;

        public void EnterFirstName(string firstName) => FindElement(_firstNameInputField).SendKeys(firstName);

        public void EnterCardCode(string cardCode) => FindElement(_cardCodeInputField).SendKeys(cardCode);

        public void EnterLastName(string lastName) => FindElement(_lastNameInputField).SendKeys(lastName);
        
        public void EnterEmail(string email) => FindElement(_emailInputField).SendKeys(email);
        
        public void EnterCompanyName(string companyName) => FindElement(_companyInputField).SendKeys(companyName);

        public void EnterCityName(string cityName) => FindElement(_cityInputField).SendKeys(cityName);

        public void EnterFirstAddress(string firstAdress) => FindElement(_newAdressOne).SendKeys(firstAdress);

        public void EnterSecondAddress(string secondAddress) => FindElement(_newAdressTwo).SendKeys(secondAddress);

        public void EnterPostalCode(string postalCode) => FindElement(_zipCode).SendKeys(postalCode);

        public void EnterPhoneNumber(string phoneNumber) => FindElement(_phoneNumberInputField).SendKeys(phoneNumber);

        public void EnterCardHolderName(string cardHolderName) => FindElement(_cardHolderNameInputField).SendKeys(cardHolderName);

        public void EnterFaxNumber(string faxNumber) => FindElement(_faxNumberInputField).SendKeys(faxNumber);

        public void EnterCardNumber(string cardNumber) => FindElement(_cardNumberInputField).SendKeys(cardNumber);

        public void ClickOnFirstContinueBtn() => FindElement(_billingContinueBtn).Click();
        
        public void ClickOnSecondContinueBtn() => FindElement(_shippingContinueBtn).Click();

        public void ClickOnConfirmBtn() => FindElement(_confirmBtn).Click();

        public void ClickOnGroundBtn() => FindElement(_groundBtn).Click();

        public void ClickOnThirdContinueBtn() => FindElement(_shippingMethodContinueBtn).Click();

        public void ClickOnFifthContinueBtn() => FindElement(_paymentInfoContinueBtn).Click();

        public void ClickOnCreditCartBtn() => FindElement(_creditcardBtn).Click();

        public void ClickOnFourthContinueBtn() => FindElement(_paymentMethodContinueBtn).Click();

        public void ClickOnSelectCreditCardBtn() => FindElement(_selectCreditCardBtn).Click();

        public void SelectCountryName()
        {
            SelectElement selectElement = new SelectElement(FindElement(_selectCountry));
            selectElement.SelectByIndex(1);
        }
    }
}
