﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    class ComputersPage : WebDriver
    {
        private static readonly By _desktopsBtn = By.XPath("//h2[@class='title']//a[@title='Show products in category Desktops']");
        
        public void ClickOnDesktopsBtn() => FindElement(_desktopsBtn).Click();
    }
}