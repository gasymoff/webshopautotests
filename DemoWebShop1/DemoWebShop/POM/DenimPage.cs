﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class DenimPage : WebDriver
    {
        private string _shoesPageUrl = "http://demowebshop.tricentis.com/apparel-shoes";
        private By _addToCardbtn = By.XPath("//div[@class='item-box'][6]/div/div[2]/div[3]/div[2]/input");

        public void OpenShoesPage() => Navigate().GoToUrl(_shoesPageUrl);
        
        public void ClickOnAddtoCard() => FindElement(_addToCardbtn).Click();
    }
}
