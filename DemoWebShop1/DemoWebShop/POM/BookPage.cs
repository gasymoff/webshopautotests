﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class BookPage : WebDriver
    {
        private static readonly By _addBookToCart = By.XPath("//div[@data-productid= '13']//input[@type = 'button']");
        private string _bookPage = "http://demowebshop.tricentis.com/books";

        public void OpenBookPage() => Navigate().GoToUrl(_bookPage);
        public void AddBookToCart() => FindElement(_addBookToCart).Click();
    }
}
