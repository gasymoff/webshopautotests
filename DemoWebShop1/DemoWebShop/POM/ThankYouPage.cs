﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class ThankYouPage : WebDriver
    {
        private static readonly By _messageInfo = By.XPath("//div[@class ='title']//strong");
        private static readonly By _continueBtn = By.XPath("//input[@class ='button-2 order-completed-continue-button']");

        public string ThankYouMessageText(string messageText) => FindElement(_messageInfo).Text.ToLower();

        public void ClickOnContinueBtn() => FindElement(_continueBtn).Click();
    }
}
