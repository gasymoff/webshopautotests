﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class DiamondPage : WebDriver
    {
        private string _diamondPageUrl = "http://demowebshop.tricentis.com/jewelry";
        private By _addToCardbtn = By.XPath("//div[@class='item-box'][2]//div[3]/div[2]/input");

        public void OpenDiaomondPage() => Navigate().GoToUrl(_diamondPageUrl);
        
        public void ClickOnAddtoCard() => FindElement(_addToCardbtn).Click();
    }
}
