﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;

namespace DemoWebShop.POM
{
    class YoutubePage : WebDriver
    {
        private static readonly By _youtubeLinkCheck = By.XPath("//ytd-searchbox[@id='search']");

        public bool CheckYoutubeLink()
        {
            ReadOnlyCollection<String> tabs2 = new ReadOnlyCollection<String>(WindowHandles);
            SwitchTo().Window(tabs2[1]);
            return FindElement(_youtubeLinkCheck).Displayed;
        }
    }
}