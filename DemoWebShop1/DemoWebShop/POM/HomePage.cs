﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class HomePage : WebDriver
    {
        private static readonly By _registrationBtn = By.XPath("//a[@class = 'ico-register']");
        private static readonly By _contactUsBtn = By.XPath("//a[@href='/contactus']");
        private static readonly By _bookCategoryBtn = By.XPath("//a[@href='/books']");
        private static readonly By _twitterLink = By.XPath("//a[contains(text(),'Twitter')]");
        private static readonly By _youtubeLink = By.XPath("//a[contains(text(),'YouTube')]");
        private static readonly By _facebookLink = By.XPath("//a[contains(text(),'Facebook')]");
        private static readonly By _computersBtn = By.XPath("//div[@class='header-menu']//ul[@class='top-menu']//a[contains(text(),'Computers')]");
        private static readonly By _authorizationBtn = By.XPath("//a[@class = 'ico-login']");
        
        public void ClickOnAuthorizationBtn() => FindElement(_authorizationBtn).Click();

        public void ClickOnRegistrationBtn() => FindElement(_registrationBtn).Click();

        public void ClickOnComputersBtn() => FindElement(_computersBtn).Click();

        public void OpenHomePage() => Navigate().GoToUrl("http://demowebshop.tricentis.com/");

        public void ClickOnContactUsBtn() => FindElement(_contactUsBtn).Click();

        public void ClickOnBookCategory() => FindElement(_bookCategoryBtn).Click();

        public void ClickOnTwitterLink() => FindElement(_twitterLink).Click();
        
        public void ClickOnYoutubeLink() => FindElement(_youtubeLink).Click();

        public void ClickFacebookLink() => FindElement(_facebookLink).Click();
    }
}
