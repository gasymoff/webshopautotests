﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace DemoWebShop.POM
{
    class DesktopsPage : WebDriver
    {
        private static readonly By _sortingList = By.XPath("//select[@id = 'products-orderby']");
        private static readonly By _firsComputerAfterSortingZtoA = By.XPath("//div[@class='product-grid']/div[1]//img");

        public void SelectSortingList()
        {
            SelectElement selectElement = new SelectElement(FindElement(_sortingList));
            selectElement.SelectByIndex(2);
        }

        public void SelectFirstComputerAfterSortingZtoA() => FindElement(_firsComputerAfterSortingZtoA).Click();
    }
}
