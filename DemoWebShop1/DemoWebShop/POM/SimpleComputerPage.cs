﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;
using System;

namespace DemoWebShop.POM
{
    class SimpleComputerPage : WebDriver
    {
        private static readonly By _addToCompareList = By.XPath("//input[@value='Add to compare list']");
        private static readonly By _firstComputerPrice = By.XPath("//span[@class='price-value-75']");

        public void SelectAddToCompareList() => FindElement(_addToCompareList).Click();

        public double GetPrice() => Convert.ToDouble(FindElement(_firstComputerPrice).Text);
    }
}