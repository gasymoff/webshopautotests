﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class ShoppingCartPage : WebDriver
    {
        private string _shoppingCartUrl = "http://demowebshop.tricentis.com/cart";
        private static readonly By _agreeTermsBtn = By.XPath("//input[@id='termsofservice']");
        private static readonly By _checkoutBtn = By.XPath("//button[@id='checkout']");

        public void OpenShoppingCartPage() => Navigate().GoToUrl(_shoppingCartUrl);

        public void ClickOnAgreeTermsBtn() => FindElement(_agreeTermsBtn).Click();

        public void ClickOnCheckoutBtn() => FindElement(_checkoutBtn).Click();
    }
}
