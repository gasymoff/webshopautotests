﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;

namespace DemoWebShop.POM
{
    public class RegistrationPage : WebDriver
    {
        private static readonly By _checkGenderMale = By.Id("gender-male");
        private static readonly By _checkGenderFemale = By.Id("gender-female");
        private static readonly By _inputFirstName = By.Id("FirstName");
        private static readonly By _inputLastName = By.Id("LastName");
        private static readonly By _inputEmail = By.Id("Email");
        private static readonly By _inputPassword = By.Id("Password");
        private static readonly By _confirmPassword = By.Id("ConfirmPassword");
        private static readonly By _registerBtn = By.Id("register-button");
        private static readonly By _firstNameErrorText = By.XPath("//span[@for='FirstName']");
        private static readonly By _lastNameErrorText = By.XPath("//span[@for='LastName']");
        private static readonly By _emailErrorText = By.XPath("//span[@for='Email']");
        private static readonly By _passwordErrorText = By.XPath("//span[@for='Password']");
        private static readonly By _confirmPasswordErrorText = By.XPath("//span[@for='ConfirmPassword']");
        private static readonly By _errorMessage = By.XPath("//div[@class='message-error']");
        private static readonly By _existEmailErrorText = By.XPath("//div[@class = 'validation-summary-errors']//ul/li");

        public void SelectGender(string gender)
        {
            if (gender == "male")
            {
                FindElement(_checkGenderMale).Click();
            }
            else
            {
                FindElement(_checkGenderFemale).Click();
            }
        }

        public void InputFirstName(string name) => FindElement(_inputFirstName).SendKeys(name);

        public void InputLastName(string lastName) => FindElement(_inputLastName).SendKeys(lastName);
        
        public void InputEmail(string email) => FindElement(_inputEmail).SendKeys(email);
        
        public void InputPassword(string password) => FindElement(_inputPassword).SendKeys(password);
        
        public void InputConfirmPassword(string confirmPassword) => FindElement(_confirmPassword).SendKeys(confirmPassword);
        
        public void ClickOnRegisterBtn() => FindElement(_registerBtn).Click();
        
        public string GetFirstNameErrorMessage() => FindElement(_firstNameErrorText).Text;
        
        public string GetLastNameErrorMessage() => FindElement(_lastNameErrorText).Text;
        
        public string GetEmailErrorMessage() => FindElement(_emailErrorText).Text;
        
        public string GetPasswordErrorMessage() => FindElement(_passwordErrorText).Text;
        
        public string GetConfirmPasswordErrorMessage() => FindElement(_confirmPasswordErrorText).Text;
        
        public bool ErrorMessageIsDisplayed() => FindElement(_errorMessage).Displayed;

        public string GetAlreadyExistErrorMessage() => FindElement(_existEmailErrorText).Text;
    }
}
