﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class AuthorizationResultPage : WebDriver
    {
        private static readonly By _authorizationResultText = By.XPath("//div[@class='header-links']//ul/li/a[@href = '/customer/info']");

        public string AuthorizationResultText() => FindElement(_authorizationResultText).Text;
    }
}
