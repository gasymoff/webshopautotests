﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    class AddGiftCardPage : WebDriver
    {
        private static readonly By _recipientsNameField = By.XPath("//input[@id='giftcard_4_RecipientName']");
        private static readonly By _addToCartBtn = By.XPath("//input[@id='add-to-cart-button-4']");
        
        public void SendDataToRecipientsNameField() => FindElement(_recipientsNameField).SendKeys("1");
        public void ClickOnAddToCartBtn() => FindElement(_addToCartBtn).Click();
    }
}
