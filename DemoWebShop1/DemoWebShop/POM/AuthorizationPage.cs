﻿using OpenQA.Selenium;
using DemoWebShop.Helpers;

namespace DemoWebShop.POM
{
    public class AuthorizationPage : WebDriver
    {
        private static readonly By _email = By.Id("Email");
        private static readonly By _password = By.Id("Password");
        private static readonly By _loginBtn = By.XPath("//input[@class = 'button-1 login-button']");
        private static readonly By _errorMessage1 = By.XPath("//div[@class='validation-summary-errors']/ul/li");
        private static readonly By _errorMessage2 = By.XPath("//span[@for='Email']");

        public void InputEmail(string email) => FindElement(_email).SendKeys(email);

        public void InputPassword(string password) => FindElement(_password).SendKeys(password);

        public void ClickOnLogInBtn() => FindElement(_loginBtn).Click();

        public string InvalidLogIn() => FindElement(_errorMessage1).Text;

        public string InvalidEmail() => FindElement(_errorMessage2).Text;
        
        public string ErrorMessageIsDisplayed(string messageText)
        {
            if(messageText== "The credentials provided are incorrect" || messageText == "No customer account found")     
            {
                return FindElement(_errorMessage1).Text;
            }
            else if(messageText == "Please enter a valid email address")
            {
                return FindElement(_errorMessage2).Text;
            }
            return null;
        }
    }
}
