﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;

namespace DemoWebShop.POM
{
    class TwitterPage : WebDriver
    {
        private static readonly By _twitterLinkCheck = By.XPath("//form[@aria-label='Поиск в Твиттере']");

        public bool CheckTwitterLink()
        {
            ReadOnlyCollection<String> tabs2 = new ReadOnlyCollection<String>(WindowHandles);
            SwitchTo().Window(tabs2[1]);
            return FindElement(_twitterLinkCheck).Displayed;
        }
    }
}