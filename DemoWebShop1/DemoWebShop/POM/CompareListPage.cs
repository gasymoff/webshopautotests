﻿using DemoWebShop.Helpers;
using OpenQA.Selenium;
using System;

namespace DemoWebShop.POM
{
    class CompareListPage : WebDriver
    {
        private static readonly By _priceInCompareList = By.XPath("//tr[@class='product-price']//td[@class='a-center']");

        public double GetPriceInCompareList() => Convert.ToDouble(FindElement(_priceInCompareList).Text);
    }
}