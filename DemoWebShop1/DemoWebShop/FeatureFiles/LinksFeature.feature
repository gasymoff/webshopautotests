﻿Feature: LinksFeature
	In order to get information about sales
	As a user
	I want to have an oppurtunity to follow the store in social media

@mytag
Scenario Outline: It is possible to follow the link in the footer
	Given 'Home' page is opened
	When I go to the <link> link
	Then Сlicked '<link>' link opened in new tab

	Examples:
		| link     |
		| Twitter  |
		| YouTube  |
		| Facebook |