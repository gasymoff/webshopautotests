﻿Feature: PurchaseMultepleItemsWithBankCard
In order to make order process more convinient and less annoying
As a user 
I want to have an opportunity add multiple products in one checkout and pay with bank card

@mytag
Scenario: It is possible to pay multiple items order with bank
	Given 'Home' page is opened
	When User adds mutliple products to shopping cart
	And Billing form is sent with data
		| firstName | lastName | company | city | adress1  | adress2   | zipCode | phoneNumber | faxNumber |
		| Gaga      | Gagayev  | gaga    | Baku | Avenue 2 | Strett 79 | AZ1033  | 99451512341 | 13131     |
	And User pay for products with cart with data
		| selectCreditCard | cardHolderName | cardNumber       | cardCode |
		| Visa             | Gaga           | 1111222233334444 | 111      |
	Then Message text 'Your order has been successfully processed!' is displayed