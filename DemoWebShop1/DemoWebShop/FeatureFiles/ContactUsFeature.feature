﻿Feature: ContactUsFeature
	In order to be in touch with Administration of the site
	As a user
	I want to send enquiry to Administration

@mytag
Scenario: It is possible to send message in web application
	Given 'ContactUs' page is opened
	When User sent 'random' generate message written on ContactUs Module
	Then Successful message with text 'Your enquiry has been successfully sent to the store owner.' is  displayed