﻿Feature: RegistrationFeature
	In order to buy computer items
	As a user 
	I want to registered 

@mytag
Scenario: It is possible to registered with valid data
	Given 'Registration' page is opened
	When I fill the form of 'registration' with data
		| gender | fname | lname | email      | password  | confirm_password |
		| male   | test  | test  | randomMail | Testtest1 | Testtest1        |
	Then I registered on the web page

@mytag
Scenario Outline: It is impossible to registered with invalid data
	Given 'Registration' page is opened
	When I fill the form of 'registration' with data
		| gender   | fname   | lname   | email   | password   | confirm_password   |
		| <gender> | <fname> | <lname> | <email> | <password> | <confirm_password> |
	Then Error message on 'registration' page with text  '<messageText>' is displayed

	Examples:
		| gender | fname       | lname | email            | password | confirm_password | messageText                                          |
		| male   |             | test  | randomMail       | testtest | testtest         | first name is required.                              |
		| male   | test        |       | randomMail       | testtest | testtest         | last name is required.                               |
		|        |             |       |                  |          |                  | first name is required.                              |
		| female | unique name | test  | testt            | testtest | testtest         | wrong email                                          |
		| male   | test        | test  | @gmail.com       | testtest | testtest         | wrong email                                          |
		| female | test        | test  | randomMail       | test     | test             | the password should have at least 6 characters.      |
		| female | test        | test  | randomMail       | testtest | test             | the password and confirmation password do not match. |
		| female | test        | test  | randomMail       |          | testtest         | password is required.                                |
		| female | test        | test  | temp01@gmail.com | testtest | testtest         | the specified email already exists                   |