﻿Feature: AutorizationWithCorrectData
	In order to enter my account
	I must enter data
	Which i filled during regestration
	So only I can enter to my account

@mytag
Scenario: It is possible to authorize with valid data
	Given 'Authorization' page is opened
	When I fill the form of 'authorization' with data
		| email      | password   |
		| gg@mail.ru | evolution7 |
	Then I authorize on the web page by account name 'gg@mail.ru'

Scenario Outline: It is impossible to authorize with invalid data
	Given 'Authorization' page is opened
	When I fill the form of 'authorization' with data
		| email   | password   |
		| <email> | <password> |
	Then Error message on 'authorization' page with text  '<messagetext>' is displayed

	Examples:
		| email        | password   | messagetext                            |
		| gg@mail.ru   | 51515121   | The credentials provided are incorrect |
		| gg@gmail.com | evolution7 | No customer account found              |
		|              |            | No customer account found              |
		| gg           | evolution7 | Please enter a valid email address     |
		| @mail.ru     | evolution7 | Please enter a valid email address     |
		|              | evolution7 | No customer account found              |
		| gg@mail.ru   |            | The credentials provided are incorrect |