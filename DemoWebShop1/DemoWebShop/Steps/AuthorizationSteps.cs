﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using DemoWebShop.Implements.General;

namespace DemoWebShop.Steps
{
    [Binding]
    public class AuthorizationSteps
    {  
        [Then(@"I authorize on the web page by account name '(.*)'")]
        public void ThenIAuthorizeOnTheWebPageByAccountName(string accountName)
        {
            Assert.AreEqual(accountName, GeneralInterfaces.AuthorizationGui.AuthorizationResultPageIsOpened());
        }
    }
}
