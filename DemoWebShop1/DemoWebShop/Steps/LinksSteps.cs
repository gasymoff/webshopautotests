﻿using DemoWebShop.Implements.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class LinksSteps
    {
        [When(@"I go to the (.*) link")]
        public void WhenIClickOnLink(string link)
        {
            GeneralInterfaces.LinksGUI.FollowToSocialLink(link.ToLower());
        }

        [Then(@"Сlicked '(.*)' link opened in new tab")]
        public void ThenСlickedLinkOpenedInNewTab(string link)
        {
            if (link.ToLower() == "facebook")
            {
                Assert.AreEqual(true, GeneralInterfaces.LinksGUI.CheckClickedLinksIsOpened(link));
            }
            else if (link.ToLower() == "twitter")
            {
                Assert.AreEqual(true, GeneralInterfaces.LinksGUI.CheckClickedLinksIsOpened(link));
            }
            else if (link.ToLower() == "youtube")
            {
                Assert.AreEqual(true, GeneralInterfaces.LinksGUI.CheckClickedLinksIsOpened(link));
            }
        }
    }
}