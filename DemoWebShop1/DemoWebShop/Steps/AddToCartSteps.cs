﻿using DemoWebShop.Implements.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class AddToCartSteps
    {
        [When(@"User add to cart a '(.*)'  book")]
        public void WhenUserAddToCartABook(string book)
        {
            GeneralInterfaces.AddToCartGui.AddToCartItem(book);
        }

        [Then(@"'(.*)' has been added to the cart")]
        public void ThenHasBeenAddedToTheCart(string book)
        {
            switch (book.ToLower())
            {
                case "computing and internet":
                    Assert.AreEqual("computing and internet", GeneralInterfaces.AddToCartGui.CheckTheNameOfBook());
                    GeneralInterfaces.AddToCartGui.ClearShopingCart();
                    break;
                case "fiction":
                    Assert.AreEqual("fiction", GeneralInterfaces.AddToCartGui.CheckTheNameOfBook());
                    GeneralInterfaces.AddToCartGui.ClearShopingCart();
                    break;
                case "health book":
                    Assert.AreEqual("health book", GeneralInterfaces.AddToCartGui.CheckTheNameOfBook());
                    GeneralInterfaces.AddToCartGui.ClearShopingCart();
                    break;
            }
        }
    }
}
