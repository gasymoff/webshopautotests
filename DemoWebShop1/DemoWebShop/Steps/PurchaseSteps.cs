﻿using NUnit.Framework;
using TechTalk.SpecFlow;
using DemoWebShop.Implements.General;

namespace DemoWebShop.Steps
{
    [Binding]
    public class PurchaseSteps
    {
        [When(@"User adds mutliple products to shopping cart")]
        public void WhenUserAddsMutlipleProductsToShoppingCart()
        {
            GeneralInterfaces.PurchaseGui.AddingItemsToTheShopCart();
        }


        [When(@"Billing form is sent with data")]
        public void WhenBillingFormIsSentWithData(Table table)
        {
            GeneralInterfaces.PurchaseGui.FillBillingData(
                table.Rows[0]["firstName"],
                table.Rows[0]["lastName"],
                table.Rows[0]["company"],
                table.Rows[0]["city"],
                table.Rows[0]["adress1"],
                table.Rows[0]["adress2"],
                table.Rows[0]["zipCode"],
                table.Rows[0]["phoneNumber"],
                table.Rows[0]["faxNumber"]);                        
        }

        [When(@"User pay for products with cart with data")]
        public void WhenUserPayForProductsWithCartWithData(Table table)
        {
            GeneralInterfaces.PurchaseGui.FillCardData(
                table.Rows[0]["cardHolderName"],
                table.Rows[0]["cardNumber"],
                table.Rows[0]["cardCode"]);
        }
       
        [Then(@"Message text '(.*)' is displayed")]
        public void ThenMessageTextIsDisplayed(string messageInfo)
        {
            Assert.AreEqual(messageInfo.ToLower(), GeneralInterfaces.PurchaseGui.MessageInfo(messageInfo));            
        }
    }
}
