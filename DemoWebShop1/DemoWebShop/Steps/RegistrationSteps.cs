﻿using DemoWebShop.Implements.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class RegistrationSteps
    {
        [Then(@"I registered on the web page")]
        public void ThenIRegisteredOnTheWebPage() => Assert.AreEqual(true, GeneralInterfaces.RegistrationGui.RegistrationResultPageIsOpened());
    }
}
