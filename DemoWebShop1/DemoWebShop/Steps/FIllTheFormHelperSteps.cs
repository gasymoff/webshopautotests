﻿using DemoWebShop.Implements.General;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class FIllTheFormHelperSteps
    {
        [When(@"I fill the form of '(.*)' with data")]
        public void WhenIFillTheFormOfWithData(string webPage, Table table)
        {
            switch (webPage.ToLower())
            {
                case "registration":
                    GeneralInterfaces.RegistrationGui.RegistrationUserWithData(
               table.Rows[0]["gender"],
               table.Rows[0]["fname"],
               table.Rows[0]["lname"],
               table.Rows[0]["email"],
               table.Rows[0]["password"],
               table.Rows[0]["confirm_password"]);
                    break;
                case "authorization":
                    GeneralInterfaces.AuthorizationGui.AuthorizationUserWithData(
                table.Rows[0]["email"],
                table.Rows[0]["password"]);
                    break;
                default:
                    break;
            }
        }
    }
}
