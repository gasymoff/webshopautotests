﻿using DemoWebShop.Implements.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class PriceInCompareListSteps
    {
        [When(@"I add a desktop to the compare list")]
        public void WhenIAddADesktopToTheCompareList()
        {
            GeneralInterfaces.PriceInCompareListGUI.AddDesktopToCompareListAfterSortingZA();
        }

        [Then(@"The price in compare list is correct")]
        public void ThenThePriceInCompareListIsCorrect()
        {
            Assert.AreEqual(true, GeneralInterfaces.PriceInCompareListGUI.CheckPriceInCompareListIsCorrect());
        }
    }
}