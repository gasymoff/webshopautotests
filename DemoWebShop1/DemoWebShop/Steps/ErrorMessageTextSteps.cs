﻿using DemoWebShop.Implements.General;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class ErrorMessageTextSteps
    {
        [Then(@"Error message on '(.*)' page with text  '(.*)' is displayed")]
        public void ThenErrorMessageOnPageWithTextIsDisplayed(string webPage, string errorMessageText)
        {
            switch (webPage)
            {
                case "registration":
                    switch (errorMessageText.ToLower())
                    {
                        case "first name is required.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                        case "last name is required.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                        case "wrong email":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                        case "the password should have at least 6 characters.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                        case "the password and confirmation password do not match.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                        case "password is required.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                        case "the specified email already exists.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.RegistrationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                    }
                    break;
                case "authorization":
                    switch (errorMessageText.ToLower())
                    {
                        case "the credentials provided are incorrect.":
                        case "no customer account found.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.AuthorizationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                        case "please enter a valid email address.":
                            Assert.AreEqual(errorMessageText, GeneralInterfaces.AuthorizationGui.ErrorMessageIsDisplayed(errorMessageText));
                            break;
                    }
                    break;
            }
        }
    }
}