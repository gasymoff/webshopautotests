﻿using DemoWebShop.Helpers;
using DemoWebShop.Implements.General;
using TechTalk.SpecFlow;

namespace DemoWebShop.Steps
{
    [Binding]
    public class OpenedPageHelperSteps
    {
        AuthorizationHelper authorizationHelper;

        public OpenedPageHelperSteps()
        {
            authorizationHelper = new AuthorizationHelper();
        }

        [Given(@"'(.*)' page is opened")]
        public void GivenPageIsOpened(string webPage)
        {
            switch (webPage.ToLower())
            {
                case "authorization":
                    GeneralInterfaces.AuthorizationGui.AuthorizationPageIsOpened();
                    break;
                case "registration":
                    GeneralInterfaces.RegistrationGui.OpenRegistrationPage();
                    break;
                case "home":
                    authorizationHelper.OpenHomePage();
                    authorizationHelper.ClickOnAuthorizationBtn().InputEmail().InputPassword().ClickOnLogInBtn();
                    break;
                case "contactus":
                    GeneralInterfaces.ContactUsGui.UserIsAuthorized();
                    break;
            }
        }
    }
}

