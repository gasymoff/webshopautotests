﻿namespace DemoWebShop.Interfaces
{
    public interface IPurchase
    {
        /// <summary>
        /// Method adds several items to shopping cart, navigates to shopping cart and goes to checkout page.
        /// </summary>
        /// 
        void AddingItemsToTheShopCart();

        /// <summary>
        /// Method checks whether checkout process was succefully completed by comparing result message text.
        /// </summary>
        /// <param name="messageText"></param>
        /// <returns></returns>
        string MessageInfo(string messageText);

        /// <summary>
        /// Method fills billing data fields with appropriate data, that is necessary to complete checkout.
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="companyName"></param>
        /// <param name="cityName"></param>
        /// <param name="firstAdress"></param>
        /// <param name="secondAddress"></param>
        /// <param name="postalCode"></param>
        /// <param name="phoneNumber"></param>
        /// <param name="faxNumber"></param>
        void FillBillingData(string firstName, string lastName, string companyName,
                             string cityName, string firstAdress, string secondAddress,
                             string postalCode, string phoneNumber, string faxNumber);

        /// <summary>
        /// Method fills card info data.
        /// </summary>
        /// <param name="cardHolderName"></param>
        /// <param name="cardNumber"></param>
        /// <param name="cardCode"></param>
        void FillCardData(string cardHolderName, string cardNumber, string cardCode);
    }
}
