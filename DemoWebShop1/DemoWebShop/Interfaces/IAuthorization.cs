﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebShop.Interfaces
{
    public interface IAuthorization
    {
        /// <summary>
        /// Method fills email and password fields with appropriate data to be log in.
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        void AuthorizationUserWithData(string email, string password);

        /// <summary>
        /// Method checks wheather authorization completed succefully.
        /// </summary>
        /// <returns></returns>
        string AuthorizationResultPageIsOpened();

        /// <summary>
        /// Method navigates to authorization page from homepage.
        /// </summary>
        void AuthorizationPageIsOpened();

        /// <summary>
        /// Method checks error message texts if there was attempt to log in with invalid/incorrect data.
        /// </summary>
        /// <param name="messageText"></param>
        /// <returns></returns>
        string ErrorMessageIsDisplayed(string messageText);

    }
}
