﻿namespace DemoWebShop.Interfaces
{
    public interface IRegistration
    {
        /// <summary>
        /// Opening registration page in web.
        /// </summary>
        void OpenRegistrationPage();

        /// <summary>
        /// Fill the form with data.
        /// </summary>
        void RegistrationUserWithData(string gender, string name, string lastName, string email,
                                      string password, string confirmPassword);

        /// <summary>
        /// Checking if registration result page opened or not.
        /// </summary>
        /// <returns></returns>
        bool RegistrationResultPageIsOpened();

        /// <summary>
        /// Checking if error message is displayed when user entered invalid data.
        /// </summary>
        /// <returns></returns>
        string ErrorMessageIsDisplayed(string errorMessageText);
    }
}
