﻿namespace DemoWebShop.Interfaces
{
    public interface IAddToCart
    {
        /// <summary>
        /// Add to cart a book from book category.
        /// </summary>
        void AddToCartItem(string book);

        /// <summary>
        /// Checking if shoping cart number change or not.
        /// </summary>
        /// <returns></returns>
        string CheckTheNameOfBook();

        /// <summary>
        /// Method helps to clear shopping cart to compare the first book's name.
        /// </summary>
        void ClearShopingCart();
    }
}
