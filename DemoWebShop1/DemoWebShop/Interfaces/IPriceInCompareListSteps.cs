﻿namespace DemoWebShop.Interfaces
{
    public interface IPriceInCompareListSteps
    {
        /// <summary>
        /// The main page of the online store opens.
        /// </summary>
        void OpenWebShopHomePage();

        /// <summary>
        /// Adding a product to the comparison list.
        /// </summary>
        void AddDesktopToCompareListAfterSortingZA();

        /// <summary>
        /// Checking the price of goods in the comparison list.
        /// </summary>
        /// <returns></returns>
        bool CheckPriceInCompareListIsCorrect();
    }
}
