﻿namespace DemoWebShop.Interfaces
{
    public interface ILinks
    {
        /// <summary>
        /// The main page of the online store opens.
        /// </summary>
        void OpenWebShopHomePage();

        /// <summary>
        /// Following social media links in the footer.
        /// </summary>
        /// <param name="link"></param>
        void FollowToSocialLink(string link);

        /// <summary>
        /// Checking for clicks on social media links in the footer.
        /// </summary>
        /// <param name="link"></param>
        /// <returns></returns>
        bool CheckClickedLinksIsOpened(string link);
    }
}