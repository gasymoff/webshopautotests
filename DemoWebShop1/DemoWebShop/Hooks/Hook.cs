﻿using DemoWebShop.Helpers;
using TechTalk.SpecFlow;

namespace DemoWebShop.Hooks
{
    [Binding]
    public class Hook:WebDriver
    {
        [AfterScenario]
        public void AfterScenario() => Quit();
    }
}
