﻿using OpenQA.Selenium;

namespace DemoWebShop.Helpers
{
    public class AuthorizationHelper : WebDriver
    {
        private static readonly By _authorizationBtn = By.XPath("//a[@href='/login']");
        private static readonly By _emailField = By.Id("Email");
        private static readonly By _passwordField = By.Id("Password");
        private static readonly By _loginBtn = By.XPath("//input[@class= 'button-1 login-button']");

        public void OpenHomePage() => Navigate().GoToUrl("http://demowebshop.tricentis.com/");

        public AuthorizationHelper InputEmail()
        {
            FindElement(_emailField).SendKeys(GlobalConst.GLOBAL_LOGIN);
            return this;
        }
        public AuthorizationHelper InputPassword()
        {
            FindElement(_passwordField).SendKeys(GlobalConst.GLOBAL_PASSWORD);
            return this;
        }

        public AuthorizationHelper ClickOnAuthorizationBtn() 
        {
            FindElement(_authorizationBtn).Click();
            return this;
        }

        public AuthorizationHelper ClickOnLogInBtn()
        {
            FindElement(_loginBtn).Click();
            return this;
        }
    }
}
