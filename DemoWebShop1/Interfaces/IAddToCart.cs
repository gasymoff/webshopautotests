﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoWebShop.Interfaces
{
    public interface IAddToCart
    {
        /// <summary>
        /// add to cart a book from book category
        /// </summary>
        void AddToCartItem(string book);

        /// <summary>
        /// checking if shoping cart number change or not
        /// </summary>
        /// <returns></returns>
        string CheckTheNameOfBook();

        /// <summary>
        /// method helps to clear shopping cart to compare the first book's name 
        /// </summary>
        void ClearShopingCart();
    }
}
