﻿namespace DemoWebShop.Interfaces
{
    public interface IContactUs
    {
        /// <summary>
        /// The user is entered exist email and password
        /// </summary>
        void UserIsAuthorized();

        /// <summary>
        /// Method generate random sentence,which has been sent by user
        /// </summary>
        void SendRandomGenerateMessage();

        /// <summary>
        /// Method put the message written by user
        /// </summary>
        /// <param name="messageText"></param>
        void SendMessageText(string messageText);

        /// <summary>
        /// checking if message was sent or not
        /// </summary>
        /// <returns></returns>
        string MessageisSended();
    }
}
