﻿using DemoWebShop.Helpers;
using TechTalk.SpecFlow;

namespace DemoWebShop.Hooks
{
    [Binding]
    public class ArgumentTransformer
    {
        [StepArgumentTransformation(@"unique (.*)")]
        public string UniqueTransformation(Table table) 
        {
            var result = ("unique" +" "+ table.Rows[0]["fname"]).Trim();

            switch (table.Rows[0]["fname"])
            {
                case "username":
                case "userName":
                case "user name":
                case "user login":
                    result = HelperMethods.GetMessage(6);
                    break;
   
            }
            ScenarioContext.Current[$"unique { table.Rows[0]["fname"]}".Trim()] = result;
            return result;
        }

        [StepArgumentTransformation(@"unique (.*)")]
        public string UsedTransformation(string value) 
        {
            return (string)ScenarioContext.Current[$"unique {value}"];
        }
    }
}
