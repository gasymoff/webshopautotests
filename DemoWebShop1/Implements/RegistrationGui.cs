﻿using DemoWebShop.Helpers;
using DemoWebShop.Interfaces;
using DemoWebShop.POM;

namespace DemoWebShop.Implements
{
    public class RegistrationGui : IRegistration
    {
        HomePage homePage;
        RegistrationPage registrationPage;
        RegistrationResultPage registrationResultPage;
        public RegistrationGui()
        {
            homePage = new HomePage();
            registrationPage = new RegistrationPage();
            registrationResultPage = new RegistrationResultPage();

        }
        public void OpenRegistrationPage()
        {
            homePage.OpenHomePage();
            homePage.ClickOnRegistrationBtn();
        }

        public bool RegistrationResultPageIsOpened()
        {
            if (registrationResultPage.RegistrationResultText().ToLower() == "your registration completed")
            {
                return true;
            }
            return false;
        }

        public string ErrorMessageIsDisplayed(string errorMessageText)
        {
            if (errorMessageText == "First name is required.")
            {
                return registrationPage.GetFirstNameErrorMessage().ToLower();
            }
            else if (errorMessageText == "Last name is required.")
            {
                return registrationPage.GetLastNameErrorMessage().ToLower();
            }
            else if (errorMessageText == "Password is required." || errorMessageText == "The password should have at least 6 characters.")
            {
                return registrationPage.GetPasswordErrorMessage().ToLower();
            }
            else if (errorMessageText == "Wrong email.")
            {
                return registrationPage.GetEmailErrorMessage().ToLower();
            }
            else if (errorMessageText == "Password is required." || errorMessageText == "The password and confirmation password do not match.")
            {
                return registrationPage.GetConfirmPasswordErrorMessage().ToLower();
            }
            return "";
        }

        public void RegistrationUserWithData(string gender, string name, string lastName, string email, string password, string confirmPassword)
        {
            registrationPage.SelectGender(gender);
            registrationPage.InputFirstName(name);
            registrationPage.InputLastName(lastName);
            if (email == "randomMail")
            {
                registrationPage.InputEmail(HelperMethods.GetUniqueEmail(7));
            }
            else
            {
                registrationPage.InputEmail(email);
            }
            registrationPage.InputPassword(password);
            registrationPage.InputConfirmPassword(confirmPassword);
            registrationPage.ClickOnRegisterBtn();
        }
    }
}
