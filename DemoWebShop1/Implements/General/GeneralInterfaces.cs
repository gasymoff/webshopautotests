﻿using DemoWebShop.Interfaces;

namespace DemoWebShop.Implements.General
{
    public static class GeneralInterfaces
    {
        public static IRegistration RegistrationGui { get; } = new RegistrationGui();

        public static IAddToCart AddToCartGui { get; } = new AddToCartGui();

        public static IContactUs ContactUsGui { get; } = new ContactUsGui();
    }
}
